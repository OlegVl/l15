package ru.ovk.home.controller;

import ru.ovk.home.service.ProjectService;
import ru.ovk.home.entity.Project;
import ru.ovk.home.exceptions.*;

public class ProjectController extends  AbstractController {

    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    public  int createProject() {
        System.out.println("[Create project]");
        System.out.println("inpunt project name");
        final String name = scaner.nextLine();
        projectService.create(name);
        System.out.println("[OK]");
        return 0;
    }

    public  int clearProject() {
        System.out.println("[Clear project]");
        projectService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public  int listProject() {
        System.out.println("[List project]");
        int index=1;
        for(final Project project: projectService.findAll()){
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public void ViewProject(final Project project) throws PrjNotFndExp {
        if(project==null) {
            throw new PrjNotFndExp("not project");
        }

        System.out.println("[View Project]");
        System.out.println( "ID: "   + project.getId());
        System.out.println( "NAME: " + project.getName());
        System.out.println( "DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

    public  int viewProjectByIndex() throws PrjNotFndExp {
        System.out.println("ENTER PROJECT INDEX");
        final int index  = scaner.nextInt() - 1;
        final Project project = projectService.findByIndex(index);
        ViewProject(project);
        return 0;
    }

    public  int viewProjectById() throws PrjNotFndExp {
        System.out.println("ENTER PROJECT ID");
        final Long id  =  scaner.nextLong();
        final Project project = projectService.findById(id);
        ViewProject(project);
        return 0;
    }

    public  int viewProjectByName() throws PrjNotFndExp {
        System.out.println("ENTER Project NAME");
        final String name  =  scaner.nextLine();
        final Project project = projectService.findByName(name);
        ViewProject(project);
        return 0;
    }

    public  int removeProjectByIndex() throws PrjNotFndExp {
        System.out.println("REMOVE Project BY INDEX");
        System.out.println("ENTER Project INDEX");
        final int index  = scaner.nextInt() - 1;
        final Project project  = projectService.removeByIndex(index);
        if(project==null) System.out.println("[FAIL]");
        else ViewProject(project);
        return 0;
    }

    public  int removeProjectById() throws PrjNotFndExp {
        System.out.println("REMOVE Project BY ID");
        System.out.println("ENTER Project ID");
        final Long id  =  scaner.nextLong();
        final Project project = projectService.removeById(id);
        if( project==null) System.out.println("[FAIL]");
        else ViewProject(project);
        return 0;
    }

    public  int removeProjectByName() throws PrjNotFndExp {
        System.out.println("ENTER Project NAME");
        final String name  =  scaner.nextLine();
        final Project project = projectService.removeByName(name);
        if(project==null) System.out.println("[FAIL]");
        else ViewProject(project);
        return 0;
    }

    public  int updateProjectById() throws PrjNotFndExp {
        System.out.println("[UPDATE Project]");
        System.out.println("PLEASE, ENTER Project ID");
        final Long id = Long.parseLong(scaner.nextLine());
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER Project NAME");
        final String name = scaner.nextLine();
        System.out.println("PLEASE, ENTER Project DESCRIPTION");
        final String description = scaner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public  int updateProjectByIndex() {
        System.out.println("[UPDATE_PROJECT]");
        System.out.println("ENTER, PROJECT INDEX:");
        final int index = Integer.parseInt(scaner.nextLine()) -1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("INPUT PROJECT NAME");
        final String name = scaner.nextLine();
        System.out.println("INPUT PROJECT DESCRIPTION:");
        final String description = scaner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

}
