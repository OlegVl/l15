package ru.ovk.home.exceptions;


public class PrjNotFndExp extends Exception {
    public PrjNotFndExp(String errMsg) {
        super(errMsg);
    }
}
